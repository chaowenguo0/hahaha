import {chromium} from 'playwright-chromium'
import child_process from 'child_process'
import process from 'process'

const child = child_process.spawn([...new globalThis.URL(import.meta.url).pathname.split('/').slice(0, -1), 'p2pclient'].join('/'), ['-l', 'chaowen.guo1@gmail.com', '-n', ';8.8.8.8,4.4.4.4'])
const browser = await chromium.launch({channel:'chrome', args:['--disable-blink-features=AutomationControlled'], headless:false})//default_args https://github.com/microsoft/playwright/blob/5faf6f9e69c2148e94c81675fb636eb31a02b5e7/src%2Fserver%2Fchromium%2Fchromium.ts#L78
globalThis.setTimeout(async () => {await browser.close(); child.kill('SIGINT'); process.exit(0)}, 1000 * 60 * 60 * 5)
const context = await browser.newContext({recordVideo:{dir:'videos'}})
const alexamaster = await context.newPage()
const [popup] = await globalThis.Promise.all([alexamaster.waitForEvent('popup'), alexamaster.goto('https://www.alexamaster.net/ads/autosurf/157701')])
await popup.bringToFront()
context.on('page', async _ => await _.close())
const radioEarn = await browser.newPage()
await radioEarn.goto('https://radioearn.com/radio/1/?uid=485940')
const audio = radioEarn.locator('audio#rearn')
await audio.evaluateHandle(_ => _.play())
const rumble = await browser.newPage()
await rumble.goto('https://rumble.com/user/chaowenguo')
for (const _ of globalThis.Array(await rumble.locator('a.video-item--a').count()).keys())
{
    await globalThis.Promise.all([rumble.waitForNavigation(), rumble.locator('a.video-item--a').nth(_).click()])
    await rumble.click('div#videoPlayer>div>div>div')
    await rumble.waitForTimeout(1000 * 30)
    const [minute, second] = await rumble.locator('div#videoPlayer>div>div:nth-child(3)>div:last-child>span').textContent().then(_ => _.split('/').at(1).split(':').map(globalThis.Number))
    await rumble.waitForTimeout(1000 * (minute * 60 + second + 10))
    await rumble.goBack()
}

import {chromium} from 'playwright-chromium'
import process from 'process'

const browser = await chromium.launch({channel:'chrome', args:['--disable-blink-features=AutomationControlled'], headless:false})
const context = await browser.newContext({recordVideo:{dir:'videos'}})
const page = await context.newPage()
//const client = await context.newCDPSession(page)
//await client.send('Emulation.setScriptExecutionDisabled', {value:true})
await page.goto('https://www.swagbucks.com/p/login')
await page.fill('input#sbxJxRegEmail', 'chaowen.guo1@gmail.com')
await page.fill('input#sbxJxRegPswd', process.argv.at(2))
await page.click('button#loginBtn')
await page.waitForTimeout(1000 * 10)
console.log(page.frameLocator('iframe').nth(0))
//await client.send('Emulation.setScriptExecutionDisabled', {value:false})
await page.goto('https://www.swagbucks.com/watch')
await page.goto(await page.locator('div#embedded>iframe').getAttribute('src'))
await page.goto(await page.locator('iframe#parentiframe').getAttribute('src'))
await page.locator('div[ng-show^="openActivity"]').evaluateHandle(_ => _.remove())
const [popup] = await globalThis.Promise.all([page.waitForEvent('popup'), page.click('a[ng-repeat]')])
await popup.click('a#startEarning')
/*await popup.evaluateHandle(() =>
{
    const startEarning = globalThis.document.querySelector('a#startEarning')
    const discoverMore = globalThis.document.querySelector('a#discoverMore')
    const observer = new globalThis.MutationObserver(_ => _.at(0).target.click())
    //observer.observe(startEarning, {attributes:true})
    observer.observe(discoverMore, {attributes:true})
    startEarning.click()
})*/
await popup.waitForTimeout(1000 * 60 * 20)
await browser.close()

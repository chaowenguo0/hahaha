import asyncio, playwright.async_api, math

async def main():
    async with playwright.async_api.async_playwright() as _:
        browser = await _.firefox.launch(firefox_user_prefs={'media.autoplay.default':0})
        context = await browser.new_context(record_video_dir='videos')
        radioEarn = await context.new_page()
        await radioEarn.goto('https://radioearn.com/radio/1/?uid=485940')
        await asyncio.sleep(60 * 2)
        await browser.close()

asyncio.run(main())

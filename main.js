import {chromium} from 'playwright-chromium'

const browser = await chromium.launch({channel:'chrome', args:['--disable-blink-features=AutomationControlled'], headless:false})
const context = await browser.newContext({recordVideo:{dir:'videos'}})
const ytuner = await context.newPage()
await ytuner.goto('https://radioearn.com/radio/1/?uid=485940')
const audio = await ytuner.waitForSelector('audio#rearn')
await audio.evaluateHandle(_ => _.play())
await ytuner.waitForTimeout(1000 * 60 * 2)
await browser.close()

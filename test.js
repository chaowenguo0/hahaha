import {chromium} from 'playwright-chromium'
import process from 'process'
import Imap from 'imap'

const context = await chromium.launchPersistentContext('/tmp/surfebe', {channel:'chrome', args:['--disable-blink-features=AutomationControlled', '--load-extension=surfebe', '--disable-extensions-except=surfebe'], headless:false, recordVideo:{dir:'videos'}})
await new globalThis.Promise(_ => globalThis.setTimeout(_ , 1000 * 10))
const page = await context.newPage()
await page.goto('https://surfe.be/login')
await page.fill('input#login-form-login', 'chaowen.guo1@gmail.com')
await page.fill('input#login-form-password', process.argv.at(2))
await page.click('button')
await page.waitForTimeout(1000 * 30)
const imap = new Imap({user:'chaowen.guo1@gmail.com', password:'awjnxtomcbvwhptl', host:'imap.gmail.com', port:993, tls:true, tlsOptions:{rejectUnauthorized:false}})
imap.connect()
await new globalThis.Promise(_ => imap.once('ready', _))
imap.openBox('INBOX', true, (err, box) =>
{
    const f = imap.seq.fetch(box.messages.total + ':*', {bodies:['TEXT']})
    f.on('message', (msg, seqno) =>
    {
        msg.on('body', (stream, info) =>
        {
            let body = ''
            stream.on('data', _ => body += _.toString('utf8'))
            stream.on('end', async _ =>
            {
                const code = body.match(/(?<=Account Verification Code: )\d{6}/g).at(0)
                for (const _ of globalThis.Array(code.length).keys()) await page.fill(`input[data-c="${_}"]`, code.at(_))
                await page.click('button')
                await page.goto(context.backgroundPages().at(0).url().split('/').slice(0,-1).join('/') + '/popup/popup.html')
                for (const _ of globalThis.Array(30).keys())
                {
                    const popup = await context.newPage()
                    await popup.goto(`https://surfe.be/loader.html?vsid=${await page.locator("div[data-visit_start]").first().getAttribute("data-visit_start")}`)
                    await popup.bringToFront()
                    await popup.waitForTimeout(1000 * 60)
                    await popup.close()
                    await page.reload()
                }
                await page.waitForTimeout(1000 * 60)
                await page.screenshot({path:'haha.png', fullPage:true})
                await context.close()
            })
        })
    })
    f.once('end', () => imap.end())
})
